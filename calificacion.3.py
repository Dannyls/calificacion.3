# ___Programa para calcular calificación cualitativa
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

try:
    puntuacion = float(input("Introduce tu calificación \n"))
# Validación del rango de puntuación
    if puntuacion >= 0 and puntuacion <= 1.0:
        if puntuacion >= 0.9:
            print("Sobresaliente")
        elif puntuacion >= 0.8:
            print("Notable")
        elif puntuacion >= 0.7:
            print("Bien")
        elif puntuacion >= 0.6:
            print("suficiente")
        elif puntuacion < 0.6:
            print("Insuficiente")
    else:
        print("Puntuacion incorrecta")
except:
    print("Ingrese únicamente números")